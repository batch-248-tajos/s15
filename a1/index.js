console.log("Hello World");

// declare variables
const fName = 'Rene Jr.';
const lName = 'Tajos';
const age = 25;
const hobbies = [
	'Coding', 'Singing', 'Dancing'
];
const workAddress = {
	houseNumber: 247,
	street: 'Camote',
	city: 'Trece Martires City',
	state: 'Cavite'
};

// log the variables
console.log('First Name: ' + fName);
console.log('Last Name: ' + lName);
console.log('Age: ' + age);
console.log('Hobbies: ');
console.log(hobbies)
console.log('Work Address: ');
console.log(workAddress);

const fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

// steve rogers age
const age2 = 40;
console.log("My current age is: " + age2);

// steve rogers friends
const friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

// steve rogers profile
const profile = {
	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,
}

// log steve rogers profile
console.log("My Full Profile: ")
console.log(profile);

// steve rogers bff
const fullName2 = "Bucky Barnes";
console.log("My bestfriend is: " + fullName2);

const fFrozenLocation = "Arctic Ocean";
console.log("I was found frozen in: " + fFrozenLocation);